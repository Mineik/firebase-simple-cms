import React from "react"
import Link from "next/link"

class cardProps {
	id:string
	title:string
	summary:string
}

export default function PostCard(props: cardProps){
	return(
		<div className="post-card">
			<h4>{props.title}</h4>
			<div className="post-summary">{props.summary}</div>
			<Link href={`/post?p=${props.id}`} as={`/post`}><a>Čti</a></Link>
		</div>
	)
}
