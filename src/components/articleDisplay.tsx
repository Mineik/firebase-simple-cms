import React from "react"
import {getPost} from "firebase/database/getPost"
import Markdown from "markdown-to-jsx"
import dynamic from "next/dynamic";

class articleProps{
id:string
}


export default function ArticleDisplay(props:articleProps){
	const [articleData, updateArticleData] = React.useState({
		title: "",
		content:"",
		summary:"",
		addedOn: undefined,
		imgUrl: null
	})

	React.useEffect(()=>{
		if(articleData.title =="") getPost(props.id, updateArticleData)
		else(console.log(articleData.title))
		})

	return(
		<div className="article-container">
			<div className="title-container">
				{articleData?.imgUrl? <img src={articleData.imgUrl} />:""}
				<h2>{articleData?.title}</h2>
			</div>
			<Markdown>{articleData?.content
				.replace("\\n", '\n')}</Markdown>
		</div>
	)

}
