import React from "react";
import "firebase/firestore"
import firebase from "firebase";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserShield} from "@fortawesome/free-solid-svg-icons";

class commentProps{
	comment:comment
}

class comment{
	comment: string
	forPost: string
	addedOn: firebase.firestore.Timestamp
	author: {
		displayName: string
		isAdmin: boolean
		imgUrl: string
	}
}

export default function Comment(props: commentProps) {
	return (
		<div className={"comment"}>
			<div className={"poster"}>
				<img src={props.comment.author.imgUrl}/>
				<span className={"authorName"}>{props.comment.author.displayName}</span>{props.comment.author.isAdmin?
				<>-<FontAwesomeIcon icon={faUserShield} title={"admin"} color={"#30ff30"}/></>:""}
				<span className={"publishTime"}>{props.comment.addedOn.toDate().toDateString()}</span>
			</div>
			<div className={"commentContent"}>
				{props.comment.comment}
			</div>
		</div>
	)
}