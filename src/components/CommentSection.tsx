import React from "react";
import {getCommentsForPost} from "firebase/database/getCommentsForPost";
import {getMeUser} from "firebase/login";
// @ts-ignore
import Comment from "components/Comment";

class CommentsProps{
	postId: string
}

export default function CommentSection(props: CommentsProps){
	const [comments, updateComments] = React.useState([])

	React.useEffect(()=>{
		if(comments.length == 0) getCommentsForPost(props.postId, updateComments)
	})

	return(
		<div className={"post-comments"}>
			{getMeUser() != null?
				<p>Here will be input field for entering your comments</p>:
				<p>You must <a title={"imagine button to popup login. its in the worx now"}>login</a> to be able to write comments</p>}
			{comments.length > 0? comments.map(commentObj=>{
				return <Comment comment={commentObj.data()} />
			}):<p>No comments for this post yet...</p>}
		</div>
	)
}