import React from "react"
import Markdown from "markdown-to-jsx"

class postProps {
	updatePostToSend
	existingPost?:Post

}

export class Post {
	title: string
	summary: string
	content: string
}



export default function NewPost(props: postProps){
	const [post, updatePost] = React.useState(new Post())

	React.useEffect(()=>{
		if(post==new Post() && existingPost) updatePost(existingPost)
	})

	return(
		<div className="newPost">
			<input value={post.title} onChange={e=>updatePost({
				title:e.target.value,
				summary:post.summary,
				content:post.content})
			/>
			<input value={post.summary} onChange={e=>updatePost({
				title: post.title,
				summary: e.target.value,
				content: post.content})
			/>
			<textarea 
				value={post.content.replace("\\n","\n")}
				onChange={e=>updatePost({
					title: post.title,
					summary: post.summary,
					content: e.target.value.replace(/\n|\r\n/gm,"\\n"})
			/>
			<button onClick={()=>updatePostToSend(post)}>{existingPost? "Update":"Publish"}</button>

			<Markdown>{post.content.replace("\\n","\n")}</Markdown>
		</div>
	)

}
