import React from "react"
import {useRouter} from "next/router"
import dynamic from "next/dynamic"
import "styling/styling.less"

const Blogpost = dynamic(()=>import("components/articleDisplay"),{ssr: false})
const Comments = dynamic(()=>import("components/CommentSection"),{ssr:false})

export default function Post(){
	const router = useRouter()
	console.log(router.query.p)
	
	return(
		<>
			<Blogpost id={router.query.p as string} />
			<Comments postId={router.query.p as string} />
		</>
	)
}
