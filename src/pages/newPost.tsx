import React from "react"
import {createPost} from "firebase/database/createPost"
import {signIn, getMeUser} from "firebase/login"
import NewPost, {Post} from "components/newPost"

export default function NewPost(){
	const [userSess,updateUserSess] = React.useState({})
	const [post,updatePost] = React.useState(new Post())
	const [email,updateEmail] = React.useState("")
	const [pass,updatePass] = React.useState("")

	React.useEffect(()=>{
		const sess = getMeUser()
		if(userSess != sess) updateUserSess(sess)
	})

	if(userSess != null) return(
	<NewPost updatePostToSend={updatePost} />
	/>
	)
	else return(
	<div>
		<h1>Login first</h1>
		<input type="text" value={email} onChange={e=>updateEmail(e.target.value)} placeholder="email" />
		<input type="password" value={pass} onChange={e=>updatePass(e.target.value)} placeholder="password" />
		<button onClick={()=>signIn(email,pass)} > Login </button>
	</div>
	)


}
