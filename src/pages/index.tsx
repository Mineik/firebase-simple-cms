import React from 'react'
import {getRecent} from "firebase/database/recentPosts"
import PostCard from "components/postCard"
import "styling/styling.less"

export default function Index(){
	const [posts, setPosts] = React.useState([])

	posts.length == 0? getRecent(setPosts):console.log("skip")
	return(
	<div>
		Welcome to my blog <br />
		Recent posts:
		<div className={"blogpostContainer"}>
			{posts.length>0? posts.map(postDoc=>{
				const post = postDoc.data()

				return <PostCard id={postDoc.id} title={post.title} summary={post.summary} />
			}):<p>There are no posts to view...</p>
			}
		</div>
	</div>
	)
}
