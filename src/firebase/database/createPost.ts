//user should be already authorised as author upon loading this script

import {app} from "firebase/init"
import * as firebase from "firebase"

export function createPost(title, summary, content){
	const publishDate = new Date()
	app.firestore().collection("blogposts").doc(`${publishDate.getFullYear()}${publishDate.getMonth()}${publishDate.getDate()}${publishDate.getHours()}${publishDate.getMinutes()}`).set({
		title: title,
		summary: summary,
		content: content,
		addedOn: firebase.firestore.Timestamp.fromDate(publishDate),
	}).then(b=>console.log("Create post successfully")).catch(console.log)
}
