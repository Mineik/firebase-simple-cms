import {app} from "firebase/init"

export function getPost(id, updateDataCallback){
	console.log(`Getting post with id: ${id}`)
	app.firestore().collection("blogposts").doc(id).get().then(doc=>{
		console.log(doc)
		console.log(doc.data())
		updateDataCallback(doc.data())
	}).catch(console.error)
}

