import {app} from "firebase/init"

export function getCommentsForPost(postID, updateDataCallback){
	console.log("getting comments")
	app.firestore().collection("comments")
		.where("forPost", "==", postID)
		.get()
		.then(data=>{
			updateDataCallback(data.docs)	// updates the component state with gotten documents
		})
		.catch(console.error)
}