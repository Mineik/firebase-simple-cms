import {app} from "firebase/init"

const auth = app.auth()

export function signUp(email,password){
	auth.createUserWithEmailAndPassword(email,password).catch(console.log)
}

export function signIn(email,password){
	auth.signInWithEmailAndPassword(email,password)
}

export function signOut(){
	auth.signOut().then(()=>console.log("logout successful"))
}

export function getMeUser(){
	return app.auth().currentUser
}

auth.onAuthStateChanged(user=>{
	if(user){
		console.log("user logged in")
	}else{
		console.log("user logged out")
	}
})


