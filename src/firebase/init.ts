const firebaseConfig = {
  apiKey: "AIzaSyAj3ibto16poK2nDbQ9CxhQ_yStU4GW8yY",
  authDomain: "mnk-blog-firebase.firebaseapp.com",
  databaseURL: "https://mnk-blog-firebase.firebaseio.com",
  projectId: "mnk-blog-firebase",
  storageBucket: "mnk-blog-firebase.appspot.com",
  messagingSenderId: "27770379575",
  appId: "1:27770379575:web:3f6d0e7af6ccd9d0ca0eb2"
};

import * as firebase from 'firebase'

export const app = firebase.initializeApp(firebaseConfig)
